import './style.less';
import React, { Component } from 'react';
import PropTypes from "prop-types";
import * as utils from "@utils";


class Tree extends Component {
  static propTypes = {
    list: PropTypes.arrayOf(PropTypes.shape({
      parent: PropTypes.number,
      id: PropTypes.number,
      title: PropTypes.string
    })),
    onChange: PropTypes.func
  };

  state = {
    list: this.props.list,
    editableItems: []
  };

  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.handleOutsideClick = this.handleOutsideClick.bind(this);
    this.onChangeThrottle = this.props.onChange ? utils.throttle(this.props.onChange, 300) : null;
  }

  componentDidMount() {
    document.addEventListener('click', this.handleOutsideClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleOutsideClick, false);
  }

  handleOutsideClick(e) {
    if (this.rootRef && !this.rootRef.contains(e.target)) {
      this.setState({
        editableItems: []
      });
    }
  }

  handleClick(id, e) {
    e.stopPropagation();

    this.setState({
      editableItems: [
        id
      ]
    });
  }

  handleChange(id, e) {
    const text = e.target.value;

    this.setState({
      list: this.state.list.map((instance) => {
        return id === instance.id ? { ...instance, title: text } : instance;
      })
    }, () => {
      this.onChangeThrottle && this.onChangeThrottle(this.state.list);
    });
  }

  handleKeyPress(e) {
    if (e.key === 'Enter') {
      this.setState({
        editableItems: []
      })
    }
  }

  isEditableItem(rootId) {
    const { editableItems } = this.state;
    return editableItems.find((id) => id === rootId) !== undefined;
  }

  getChildNodes(rootId) {
    const { list } = this.state;
    const filteredList = list.filter(({ parent }) => (rootId === parent));

    if (!filteredList.length) return null;

    return <ul className="tree-list__sublist">
      {
        filteredList
          .map(({ id, title}) => {
            return !this.isEditableItem(id) ? <li className="tree-list__subitem" key={ id } onClick={ (e) => this.handleClick(id, e) } >
              { title }
              { this.getChildNodes(id, list) }
            </li> : <li className="tree-list__subitem" key={ id }>
              <input
                     className="tree-list__input"
                     value={ title }
                     onChange={ (e) => this.handleChange(id, e) }
                     onKeyPress={ this.handleKeyPress }
                     onClick={ (e) => e.stopPropagation() }
                     autoFocus
              />
              { this.getChildNodes(id, list) }
            </li>
          })
      }
    </ul>
  }

  render() {
    const { list } = this.state;

    return <ul className="tree-list" ref={ (ref) => this.rootRef = ref }>
      { list.map((item) => {
        const { id, title, parent } = item;

        if (!parent) {
          return <li className="tree-list__item" key={ id }>
            { title }
            { this.getChildNodes(id, list) }
          </li>
        }

        return null;
      }) }
    </ul>
  }
}

export default Tree;
