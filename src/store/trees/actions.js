import * as api from '../../api';

export const types = {
  GET_PENDING: Symbol('SUBMIT'),
  GET_SUCCESS: Symbol('SUBMIT_SUCCESS'),
  GET_FAILURE: Symbol('SUBMIT_FAILURE'),
  PUT_PENDING: Symbol('PUT_PENDING'),
  PUT_SUCCESS: Symbol('PUT_SUCCESS'),
  PUT_FAILURE: Symbol('PUT_FAILURE'),
};


const actions = {
  get: () => {
    return async dispatch => {
      dispatch({ type: types.GET_PENDING});

      try {
        const data = await api.trees.list();
        dispatch({ type: types.GET_SUCCESS, payload: data.data });
      } catch (e) {
        dispatch({ type: types.GET_FAILURE, errors: e });
      }
    };
  },
  put: (list) => {
    return async dispatch => {
      dispatch({ type: types.PUT_PENDING});

      try {
        const data = await api.trees.put(list);
        dispatch({ type: types.PUT_SUCCESS, payload: data.data });
      } catch (e) {
        dispatch({ type: types.GET_FAILURE, errors: e });
      }
    };
  }
};

export default actions;
