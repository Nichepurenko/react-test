import reducer from '../../utils/reducer';
import { types } from './actions.js';

const initState = {
  data: [],
  wait: false,
  errors: null,
};

export default reducer(initState, {
  [types.GET_PENDING]: (state, action) => {
    return {
      ...state,
      wait: true,
      data: action.payload,
    };
  },
  [types.GET_SUCCESS]: (state, action) => {
    return {
      ...state,
      wait: false,
      data: action.payload,
    };
  },
  [types.GET_FAILURE]: (state, action) => {
    return {
      ...state,
      wait: false,
      data: [],
      errors: action.errors
    };
  },
});
