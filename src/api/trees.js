import http from '../utils/http';
import config from '../config';

export default {
  /**
   * Trees
   * @returns {Promise}
   */
  list: () => {
    return http.get(`${config.mockAPI}`);
  },
  put: (list) => {
    return http.put(`${config.mockAPI}`, list);
  }
}
