import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Route, BrowserRouter, Switch } from 'react-router-dom';

import '../../theme/style.less';
import Home from '../pages/home';
import NotFound from '../pages/not-found';


class App extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <Fragment>
        <BrowserRouter basename="/react-test">
          <Switch>
            <Route path="/" exact={true} component={Home} />
            <Route component={NotFound} />
          </Switch>
        </BrowserRouter>
      </Fragment>
    );
  }
}

export default connect()(App);
