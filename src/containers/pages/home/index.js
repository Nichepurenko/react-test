import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Accordion from '@components/elements/accordion';
import LayoutPage from '@components/layouts/layout-page';
import LayoutContent from '@components/layouts/layout-content';
import HeaderContainer from '@containers/header-container';
import TreeContainer from '@containers/trees-container';

class Home extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
  };

  render() {
    return (
      <LayoutPage header={<HeaderContainer />}>
        <LayoutContent>
          <h1>Главная страница</h1>
          <Accordion title={'Tree'}>
            <TreeContainer/>
          </Accordion>
        </LayoutContent>
      </LayoutPage>
    );
  }
}

export default connect()(Home);
