import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from '@store/actions';
import Tree from '@components/elements/tree';


class TreeContainer extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.dispatch(actions.trees.get());
  }

  render() {
    const { trees } = this.props;
    const { data, errors, wait } = trees;

    if (wait && !errors) return <p>Loading...</p>;
    if (errors) return <p>Something goes wrong...</p>;

    return <Tree list={ data } onChange={ (list) => {
      this.props.dispatch(actions.trees.put(list));
    }}/>
  }
}

export default connect(state => ({
  trees: state.trees,
}))(TreeContainer);
